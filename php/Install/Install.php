<?php

class Install
{
    public static function checkInstallationScripts()
    {
        $currentInstallationVersion = self::getVersionNumber();
        $newInstallationVersion = $currentInstallationVersion;

        foreach(glob(PROJECT_DIR. '/Install/install-*') as $script)
        {
            $scriptVersion = self::getScriptVersionNumber($script);

            if($scriptVersion > $currentInstallationVersion)
            {
                include_once $script;

                if($scriptVersion > $currentInstallationVersion)
                {
                    $newInstallationVersion = $scriptVersion;
                }
            }
        }
        if($newInstallationVersion > $currentInstallationVersion)
        {
            self::updateInstallationVersion($newInstallationVersion);
        }
    }

    private static function getVersionNumber()
    {
        $db = Connector::getInstance();
        $query = 'SHOW TABLES';
        $result = $db->query($query);
        if($result->getCount() > 0)
        {
            $query = 'SELECT value AS version FROM configurations WHERE config = "version"';
            $result = $db->query($query);
            $version = $result->firstItem();

            return $version->getData('version');
        }

        return 0;
    }

    private static function getScriptVersionNumber($fileName)
    {
        list( ,$versionString) = explode('-', $fileName);
        $versionString = substr($versionString, 0, strlen($versionString) - 4);
        $power = 1;
        $version = 0;
        foreach (array_reverse(explode('.', $versionString)) as $number)
        {
            $version += $number*$power;
            $power *= 1000;
        }

        return $version;
    }

    private static function updateInstallationVersion($newVersion)
    {
        $db = Connector::getInstance();
        $query = "INSERT INTO configurations (config, value) VALUES('version', $newVersion) ON DUPLICATE KEY UPDATE value = VALUES(value)";
        try {
            $db->rawSqlQuery($query);
        }catch (Exception $e)
        {
            var_dump($e->getMessage());
        }
    }
}