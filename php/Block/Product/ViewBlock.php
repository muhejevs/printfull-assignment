<?php

class ProductViewBlock extends BaseBlock
{
    public function getProductCollection()
    {
        $collection = new ProductCollection();
        return $collection->getCollection();
    }

    public function getProductsAttributeHtml($product)
    {
        $attributeSetId = $product->getData('attribute_set_id');
        $block = new BaseBlock();
        $template = "product/attribute_set/set$attributeSetId.phtml";
        $block->setTemplate($template);
        $block->setData('product', $product);

        return $block->toHtml();
    }

    public function priceFormat($price)
    {
        return number_format((float)$price, 2)."$";
    }
}