<?php

class RequestParser
{
    private $url = null;
    private $controller = null;
    private $params = null;

    public function __construct()
    {
        $this->parse();
    }

    private function parse()
    {
        $this->normalizeUrl();
        $this->setController();
        $this->setParams();
    }

    public function getController()
    {
        return $this->controller;
    }

    public function assignParams()
    {
        $_GET['url_param'] = $this->params;
    }

    private function normalizeUrl()
    {
        $requestUri = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'];
        $scriptName = $_SERVER['SCRIPT_FILENAME'];
        $params = explode('/', $requestUri);
        $scriptNameParams = explode('/', $scriptName);

        $urlData = array_values(array_filter($params));
        $scriptNameData = array_values(array_filter($scriptNameParams));

        $urlDataCount = count($urlData);
        $break = false;
        foreach ($scriptNameData as $key => $value)
        {
            if ($break || $urlDataCount <= $key)
            {
                break;
            }

            if ($value == $urlData[0])
            {
                array_shift($urlData);
            } else
            {
                $break = true;
            }
        }
        $this->url = $urlData;
    }

    private function setController()
    {
        if(isset($this->url[0]))
        {
            $this->controller = array_shift($this->url);
        }
    }

    private function setParams()
    {
        $urlData = $this->url;
        $functionParams = array();
        while(count($urlData) > 0)
        {
            if(isset($urlData[0]) && isset($urlData[1]))
            {
                $paramKey = array_shift($urlData);
                $paramValue = array_shift($urlData);
                $functionParams[$paramKey] = $paramValue;
            }
            else
            {
                break;
            }
        }

        $this->url = $urlData;
        $this->params = $functionParams;
    }
}