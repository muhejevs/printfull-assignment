<?php
$db = Connector::getInstance();

$query = "
CREATE TABLE configurations
(
    config_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    config varchar(50) NOT NULL UNIQUE,
    value varchar(50)
);
";

try {
    $db->rawSqlQuery($query);
} catch (Exception $e)
{
    var_dump($e->getMessage());
}