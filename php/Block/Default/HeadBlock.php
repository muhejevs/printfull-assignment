<?php

class HeadBlock extends BaseBlock
{
    private $cssFiles = array();
    private $jsFiles = array();
    private $title = null;

    public function addCss($path)
    {
        $this->cssFiles[] = $path;
    }

    public function addJs($path)
    {
        $this->jsFiles[] = $path;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getCssFilesHtml()
    {
        $html = "";

        foreach ($this->cssFiles as $cssFile)
        {
            $html .= "<link rel='stylesheet' type='text/css' href='{$this->getUrl($cssFile)}' />";
        }

        return $html;
    }

    public function getJsFilesHtml()
    {
        $html = "";

        foreach ($this->jsFiles as $jsFile)
        {
            $html .= "<script type='text/javascript' src='{$this->getUrl($jsFile)}' ></script>";
        }

        return $html;
    }
}