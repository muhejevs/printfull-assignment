<?php

class Main
{
    private static $data = array();

    public function run()
    {
        $this->loadOnFiles();

        Install::checkInstallationScripts();
        $router = new Router();

        /* @var AbstractController $controller */
        $controller = $router->getController();
        $controller->execute();
    }

    public function loadOnFiles()
    {
        $xml = simplexml_load_file(PROJECT_DIR."etc/files.xml");
        $files = $xml->files;
        $files = $files[0]->file;
        $count = count($files);
        for ($i = 0; $i < $count; $i++)
        {
            include_once((string)$files[$i]);
        }

    }
    public static function register($key)
    {
        return (isset(self::$data[$key])) ? self::$data[$key] : null;
    }

    public static function setRegister($key, $value)
    {
        self::$data[$key] = $value;
    }
}