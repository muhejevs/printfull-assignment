<?php

class ProductCollection extends Product
{
    private $query = null;
    private $collection = array();

    public function getCollection($conditions = array())
    {
        $this->loadMainTableData();
        $this->loadModelAttributes();
        $this->addFilter($conditions);

        return $this->loadCollection();
    }

    protected function loadMainTableData()
    {
        $this->query = "SELECT {$this->baseTable}.* FROM {$this->baseTable}";
    }

    protected function loadModelAttributes()
    {
        $query = "SELECT * FROM attribute";
        $db = Connector::getInstance();
        $attributes = $db->query($query)->getCollection();

        $previousTable = 'main_table';
        foreach ($attributes as $attribute)
        {
            $attributeId = $attribute->getData('id');
            $attributeType = $attribute->getData('attribute_type');
            $attributeCode = $attribute->getData('code');

            $attributeTable = "product_data_$attributeType";
            $attributeTableAlias = $attributeTable."_".$attributeCode;
            $this->query = "SELECT $previousTable.*, $attributeTableAlias.value AS $attributeCode 
                FROM ($this->query) AS $previousTable
                  LEFT JOIN $attributeTable AS $attributeTableAlias
                  ON $attributeTableAlias.product_id = $previousTable.product_id AND $attributeTableAlias.attribute_id = $attributeId";
            $previousTable = $attributeTableAlias;
        }

    }

    protected function addFilter($conditions)
    {
        $conditionArray = array();
        foreach ($conditions as $condition)
        {
            $columnName = $condition['attribute'];
            $conditionOperator = $condition['operator'];
            $conditionValue = $condition['value'];

            $conditionArray[] = "($columnName $conditionOperator $conditionValue)";
        }
        $conditionString = implode(' AND ', $conditionArray);

        if(strlen($conditionString))
        {
            $this->query .= " WHERE $conditionString";
        }
        $this->query .= " ORDER BY product_id";
    }

    protected function loadCollection()
    {
        $db = Connector::getInstance();
        $this->collection = array();
        foreach ($db->query($this->query)->getCollection() as $product)
        {
            $productObject = new Product();
            $productObject->addData($product->getData());
            $this->collection[] = $productObject;
        }
        return $this->collection;
    }
}