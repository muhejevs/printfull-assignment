<?php

class Router
{
    public function getController()
    {
        $requestParser = new RequestParser();
        $controller = $requestParser->getController();
        $requestParser->assignParams();

        $controller = ($controller == '') ? 'Index' :  ucfirst($controller);
        $controllerName = "{$controller}Controller";
        if (class_exists($controllerName))
        {
            return new $controllerName();
        }

        return new LandingController();

    }
}