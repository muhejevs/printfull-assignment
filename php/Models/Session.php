<?php

class Session extends DataObject
{
    private static $instance = null;


    private function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_save_path(PROJECT_DIR."Sessions");
            session_start();
        }
    }

    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getMessage($key)
    {
        self::getInstance();
        if(isset($_SESSION['messages'][$key]))
        {
            $message = $_SESSION['messages'][$key];
            unset($_SESSION['messages'][$key]);
            return $message;
        }

        return null;
    }

    public static function setMessage($key, $message)
    {
        self::getInstance();

        $_SESSION['messages'][$key][] = $message;
    }

    public static function getMessages()
    {
        self::getInstance();
        if(isset($_SESSION['messages']))
        {
            $messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
            return $messages;
        }

        return array();
    }
}