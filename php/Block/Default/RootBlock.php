<?php

class RootBlock extends BaseBlock
{
    public function getLayoutIdentifier()
    {
        $layoutIdentifier = Main::register('layout_identifier');
        if($layoutIdentifier != null)
        {
            $layoutIdentifier = explode('_', $layoutIdentifier);
            return implode('-', $layoutIdentifier);
        }
    }
}