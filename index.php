<?php

define('ROOT_DIR', __DIR__);
define('PROJECT_DIR', ROOT_DIR.'/php/');
define('PROJECT_TEMPLATES', ROOT_DIR.'/templates/');

include_once PROJECT_DIR.'main.php';


$app = new Main();
$app->run();
