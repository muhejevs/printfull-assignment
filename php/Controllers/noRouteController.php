<?php

class noRouteController extends AbstractController
{
    public function execute()
    {
        parent::execute();

        Session::setMessage('error', 'Error Page');
        $this->setRedirect('page_404');
    }
}