<?php

class AbstractController implements ControllerInterface
{
    protected $post = null;
    protected $get = null;

    public function __construct()
    {
        $this->post = $_POST;
        $this->get = $_GET;
    }

    public function execute()
    {
        $this->renderResponse();
    }

    protected function renderResponse()
    {
        $structure = LayoutCompiler::readLayoutSettings();
        $pageStructure = $this->getPageStructure($structure);

        $html = '';
        foreach ($pageStructure as $blockName => $blockParams)
        {
            /* @var $block BaseBlock */
            $actions = isset($blockParams['actions']) ? $blockParams['actions'] : null;
            $childBlocks = isset($blockParams['child_blocks']) ? $blockParams['child_blocks'] : null;
            $block = new $blockParams['class']($childBlocks, $actions, $blockParams['template']);
            $html .= $block->toHtml();
        }
        echo $html;
    }

    public function getPageStructure($structure)
    {
        $layoutIdentifier = $this->getLayoutIdentifier();

        $finalLayoutIdentifier = 'default';
        $defaultStructure = $structure[$finalLayoutIdentifier];

        $finalLayoutIdentifier = $this->getCurrentPageLayout($structure, $layoutIdentifier);

        $pageStructure = $structure[$finalLayoutIdentifier];
        Main::setRegister('layout_identifier', $finalLayoutIdentifier);

        $mergeStructure = array_merge($pageStructure, $defaultStructure);
        if(isset($mergeStructure['references']))
        {
            $references = $mergeStructure['references'];
            unset($mergeStructure['references']);

            foreach ($mergeStructure as $name => $block)
            {
                $mergedBlock = $this->referenceMerge($references, $block, $name);
                $mergeStructure[$name] = $mergedBlock;
            }
        }

        return $mergeStructure;
    }

    private function referenceMerge($references, $structure, $name)
    {
        if(isset($references[$name]))
        {
            $reference = $references[$name];
            if(isset($reference['action']))
            {
                $structureAction = (isset($structure['action'])) ? $structure['action'] : array();
                $mergedActions = array_merge($structureAction, $reference['action']);
                $structure['action'] = $mergedActions;
                unset($reference['action']);
            }
            foreach ($reference as $blockName => $blockDetail)
            {
                $structure['child_blocks'][$blockName] = $blockDetail;
            }
        }

        if(isset($structure['child_blocks']))
        {
            foreach ($structure['child_blocks'] as $name => $block)
            {
                $mergedBlock = $this->referenceMerge($references, $block, $name);
                $structure['child_blocks'][$name] = $mergedBlock;
            }
        }


        return $structure;
    }

    protected function  getLayoutIdentifier()
    {
        $controller = get_class($this);

        if($controller == 'IndexController')
        {
            $layoutIdentifier = 'home_page';
        }
        else
        {
            $layoutIdentifier = $this->calculateLayoutIdentifier($controller);
        }

        return $layoutIdentifier;
    }

    protected function setRedirect($url = null)
    {
        $url = $this->getUrl($url);
        header('Location: '.$url);
    }

    protected function getUrl($url = null)
    {
        $xml = simplexml_load_file(PROJECT_DIR."etc/config.xml");
        $server = $xml->server[0];
        $baseUrl = (string)$server->url;

        return $baseUrl."/".$url;
    }

    /**
     * @param $structure
     * @param $layoutIdentifier
     * @return mixed
     */
    protected function getCurrentPageLayout($structure, $layoutIdentifier)
    {
        return (isset($structure[$layoutIdentifier])) ? $layoutIdentifier : 'default';
    }

    private function calculateLayoutIdentifier($controller)
    {
        $layoutIdentifier = preg_split('/(?=[A-Z])/',$controller);
        $layoutIdentifier = array_values(array_filter($layoutIdentifier));


        if (($key = array_search('Controller', $layoutIdentifier)) !== false)
        {
            unset($layoutIdentifier[$key]);
        }

        return strtolower(implode("_", $layoutIdentifier));
    }

    protected function cleanData($data)
    {
        $result = "";
        $db = Connector::getInstance();
        if(is_array($data))
        {
            $result = array();
            foreach ($data as $row)
            {
                $result[] = $db->realEscapeString($row);
            }
        }
        return $result;
    }
}