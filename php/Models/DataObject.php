<?php

class DataObject
{
    private $data = array();

    public function addData($data)
    {
        if(is_array($data))
        {
            array_filter($data, function($value) { return $value === ''; });
            $this->data = (array)$data;
            return $this;
        }

        return null;
    }

    public function getData($key = null)
    {
        if($key == null)
        {
            return $this->data;
        }
        if(isset($this->data[$key]))
        {
            return $this->data[$key];
        }
        return null;
    }

    public function setData($key, $value = null)
    {
        if($key != null)
        {
            $this->data[$key] = $value;
        }
        return $this;
    }
}