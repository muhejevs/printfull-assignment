<?php

$db = Connector::getInstance();

$query = "
CREATE TABLE quiz
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL
);
";

try {
    $db->rawSqlQuery($query);
} catch (Exception $e)
{
    var_dump($e->getMessage());
}
$query = "
CREATE TABLE questions
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    quiz_id int NOT NULL,
    question_text varchar(200) NOT NULL,
    position int,
    CONSTRAINT questions_quiz_id_fk FOREIGN KEY (quiz_id) REFERENCES quiz (id)
);
";

try {
    $db->rawSqlQuery($query);
} catch (Exception $e)
{
    var_dump($e->getMessage());
}
$query = "
CREATE TABLE question_options
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    question_id int NOT NULL,
    option_text varchar(50) NOT NULL,
    CONSTRAINT question_options_questions_id_fk FOREIGN KEY (question_id) REFERENCES questions (id)
);
";

try {
    $db->rawSqlQuery($query);
} catch (Exception $e)
{
    var_dump($e->getMessage());
}
$query = "
CREATE TABLE question_correct_options
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    question_option_id int,
    CONSTRAINT question_correct_options_question_options_id_fk FOREIGN KEY (question_option_id) REFERENCES question_options (id)
);
";

try {
    $db->rawSqlQuery($query);
} catch (Exception $e)
{
    var_dump($e->getMessage());
}