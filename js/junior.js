var junior = {
    initialize: function() {
        $('#product-type').on('change', function (e) {
            var target = $(e.target);

            var productType = target.val();

            if(productType !== "")
            {
                $('.product-type').hide();
                $('.product-type input').each(function (i, el) {
                    $(el).removeAttr('required').val('');
                });
                $('.product-type.' + productType).show();
                $('.product-type.' + productType + ' input').each(function (i, el) {
                    $(el).attr('required', 'required');
                });
            }
        });

        $('#save-product-submit').on('click', function() {
            $('form').addClass('validation');
            $('.submit-button').click();
        });
        $('form').on('submit', function(form){
            form.addClass('validation');
            var target = form.target;
            if(!target.checkValidity()) {
                return false;
            }
        });

        $('#mass-action-submit').on('click', function() {
            var massActions = $('#mass-actions');
            var sku = [];
            $('.mass-delete:checked').each(function() {
                sku.push($(this).val());
            });

            var url = massActions.val();
            if(url.length > 0)
            {
                $.ajax({
                    type: 'POST',
                    url: massActions.val(),
                    data: {sku: sku},
                    success: function (data) {
                        $('body').html(data);
                        junior.initialize();
                    },
                    error: function (data) {
                        console.log(data);
                    }


                });
            }
        });
    }
};
$(window).ready(function () {
    junior.initialize();
});
