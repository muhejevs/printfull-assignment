<?php

class ProductNewBlock extends BaseBlock
{
    public function getFormUrl()
    {
        return $this->getUrl('productCreate');
    }
}