<?php

class Product extends BaseModel
{
    public function __construct()
    {
        $this->baseTable = 'product';
        $this->tableFields = array('name', 'sku', 'price', 'attribute_set_id');
    }

    public function save()
    {
        $attributeSetId = $this->getData('attribute_set_id');
        if($attributeSetId == null)
        {
            $attributeSetCode = $this->getData('product-type');
            $attributeSetId = $this->assignAttributeSet($attributeSetCode);
        }

        if($attributeSetId != null)
        {
            $attributes = $this->getModelAttributes($attributeSetId);
            $this->validateAttributes($attributes);

            parent::save();
            foreach ($attributes as $attribute)
            {
                $this->saveAttribute($attribute);
            }
        }
    }
}