<?php

class ProductCreateController extends AbstractController
{
    public function execute()
    {
        $product = new Product();
        $product->addData($this->post);

        try
        {
            $product->save();
            Session::setMessage('success', 'Success');
        }
        catch (Exception $e)
        {
            Session::setMessage('error', $e->getMessage());
        }


        $this->setRedirect('productNew');
    }
}