<?php

class LandingController extends AbstractController
{
    protected function getLayoutIdentifier()
    {
        $requestParser = new RequestParser();
        $landingPage = $requestParser->getController();

        return $landingPage;
    }

    protected function getCurrentPageLayout($structure, $layoutIdentifier)
    {
        return (isset($structure[$layoutIdentifier])) ? $layoutIdentifier : 'page_404';
    }
}