<?php

class ProductMassDeleteController extends AbstractController
{
    public function execute()
    {
        if(isset($this->post['sku']))
        {
            $data = $this->cleanData($this->post['sku']);
            $sku = "'".implode("','", $data)."'";
            $collection = new ProductCollection();
            $filter = array(
                array('attribute' => 'sku', 'operator' => 'IN', 'value' => "(".$sku.")")
            );
            $collection = $collection->getCollection($filter);
            try
            {
                foreach ($collection as $product)
                {
                    $product->delete();
                }
                $this->setRedirect('productView');
            }
            catch (Exception $e)
            {
                echo json_encode(array('error' => true, 'message' => $e->getMessage()));
            }

        }
        else
        {
            echo json_encode(array('error' => true, 'message' => 'Products were not selected'));
        }
    }
}