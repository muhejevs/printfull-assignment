<?php

class BaseBlock extends DataObject
{
    protected $template = null;
    protected $childBlocks = null;
    protected $actions = null;

    public function __construct($childBlocks = null, $actions = null, $template = null)
    {
        $this->childBlocks = $childBlocks;
        if($template != null)
        {
            $this->setTemplate($template);
        }

        if($actions !== null)
        {
            foreach ($actions as $action)
            {
                $method = $action['method'];
                $args = $action['params'];
                call_user_func_array(array($this, $method), $args);
            }
        }
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function toHtml()
    {
        ob_start();
        include PROJECT_TEMPLATES.$this->template;
        return ob_get_clean();
    }

    public function getChildBlockHtml($name)
    {

        if(!isset($this->childBlocks[$name]))
        {
            return '';
        }

        $blockParams = $this->childBlocks[$name];
        /* @var $block BaseBlock */
        $actions = isset($blockParams['action']) ? $blockParams['action'] : null;
        $childBlocks = isset($blockParams['child_blocks']) ? $blockParams['child_blocks'] : null;
        $block = new $blockParams['class']($childBlocks, $actions, $blockParams['template']);
        return $block->toHtml();
    }

    protected function getUrl($url = null)
    {
        $xml = simplexml_load_file(PROJECT_DIR."etc/config.xml");
        $server = $xml->server[0];
        $baseUrl = (string)$server->url;

        return $baseUrl."/".$url;
    }
}