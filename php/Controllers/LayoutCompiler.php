<?php

class LayoutCompiler
{
    public static function readLayoutSettings()
    {
        $xml = simplexml_load_file(PROJECT_TEMPLATES."layout.xml");
        $structure = array();
        foreach ($xml as $pageName => $page)
        {
            $structure[$pageName] = self::layoutRead($page);
        }

        return $structure;
    }

    private static function layoutRead($blockXml)
    {
        $structure = array();
        $references = array();
        foreach ($blockXml as $type => $childXml)
        {
            $elementAttribute = self::getXmlAttributes($childXml);
            if($type == 'block')
            {
                $result = self::layoutRead($childXml);
                if(isset($result['action']))
                {
                    $elementAttribute['action'] = $result['action'];
                    unset($result['action']);
                }

                $elementAttribute['child_blocks'] = $result;

                $blockName = $elementAttribute['name'];
                unset($elementAttribute['name']);
                $structure[$blockName] = $elementAttribute;
            }
            if($type == 'action')
            {
                foreach ($childXml as $paramName => $value)
                {
                    $elementAttribute['params'][$paramName] = (string)$value;
                }

                $structure['action'][] = $elementAttribute;
            }
            if($type == 'reference')
            {
                $result = self::layoutRead($childXml);
                $blockName = $elementAttribute['name'];
                $references[$blockName] = $result;
            }

        }
        if(count($references))
        {
            $structure['references'] = $references;
        }


        return $structure;
    }

    private static function getXmlAttributes($xmlBlock)
    {
        $attributes = array();
        foreach($xmlBlock->attributes() as $key => $value)
        {
            $attributes[$key] = (string)$value;
        }
        return $attributes;
    }
}