<?php

class BaseModel extends DataObject
{
    protected $baseTable = null;
    protected $tableFields = array();

    public function     load($id)
    {
        $this->loadMainTable($id);
        $this->modelAttributeLoad();
        return $this;
    }

    protected function loadMainTable($id)
    {
        $db = Connector::getInstance();
        $query = "SELECT * FROM {$this->baseTable} WHERE product_id = $id";
        $modelTable = $db->query($query)->firstItem();
        foreach ($this->tableFields as $name)
        {
            $value = $modelTable->getData($name);
            $this->setData($name, $value);
        }
        $this->setData('product_id', $modelTable->getData('product_id'));
    }

    protected function modelAttributeLoad()
    {
        $attributeSetId = $this->getData('attribute_set_id');
        $attributes = $this->getModelAttributes($attributeSetId);

        foreach ($attributes as $attribute)
        {
            $this->loadAttribute($attribute);
        }
    }

    public function save()
    {
        try
        {
            if($this->getData('product_id'))
            {
                $this->updateRecord();
            }
            else
            {
                $modelId = $this->addRecord();
                $this->setData('product_id', $modelId);
            }
        }
        catch (Exception $e)
        {
            throw new Exception($e->getMessage());
        }

        return $this;
    }

    private function addRecord()
    {
        $db = Connector::getInstance();
        $query = "INSERT INTO {$this->baseTable} ";

        $names = array();
        $values = array();
        foreach ($this->tableFields as $name)
        {
            $value = $db->realEscapeString($this->getData($name));

            if(empty($value))
            {
                throw new Exception("$name is empty");
            }

            if($value !== null)
            {
                $names[] = $name;
                $values[] = $value;
            }
        }

        $names = implode(",", $names);
        $query .= "($names)";
        $values = implode("','", $values);
        $query .= " VALUES ('$values')";

        return $db->insert($query);
    }

    private function updateRecord()
    {
        $db = Connector::getInstance();
        $query = "UPDATE {$this->baseTable} SET ";

        $fields = array();
        foreach ($this->tableFields as $name)
        {
            $value = $db->realEscapeString($this->getData($name));

            $fields[] = "$name = '$value'";
        }
        $query .= implode(',', $fields);
        $query .= " WHERE product_id = ".$this->getData('product_id');

        return $db->rawSqlQuery($query);
    }

    protected function assignAttributeSet($attributeSetCode)
    {
        $db = Connector::getInstance();

        $query = "SELECT * FROM attribute_set WHERE code = '$attributeSetCode'" ;
        $result = $db->query($query);
        $attributeSetId = $result->firstItem()->getData('id');

        $this->setData('attribute_set_id', $attributeSetId);
        return $attributeSetId;
    }

    protected function loadAttribute($attribute)
    {
        $db = Connector::getInstance();

        $type = $attribute->getData('attribute_type');
        $tableName = "{$this->baseTable}_data_$type";
        $code = $attribute->getData('code');

        $attributeId = $attribute->getData('id');
        $modelId = $this->getData('product_id');

        $query = "SELECT value FROM $tableName WHERE {$this->baseTable}_id = $modelId AND attribute_id = $attributeId";
        $value = $db->query($query)->firstItem()->getData('value');
        $this->setData($code, $value);
    }

    protected function saveAttribute($attribute)
    {
        $db = Connector::getInstance();

        $type = $attribute->getData('attribute_type');
        $tableName = "{$this->baseTable}_data_$type";
        $code = $attribute->getData('code');

        $attributeId = $attribute->getData('id');
        $modelId = $this->getData('product_id');
        $value = $db->realEscapeString($this->getData($code));
        $query = "INSERT INTO $tableName (attribute_id, {$this->baseTable}_id, value) 
                    VALUES($attributeId, $modelId, '$value') ON DUPLICATE KEY UPDATE    
                    value = '$value'";
        $db->insert($query);
    }

    protected function deleteAttribute($attribute)
    {
        $db = Connector::getInstance();

        $type = $attribute->getData('attribute_type');
        $tableName = "{$this->baseTable}_data_$type";

        $attributeId = $attribute->getData('id');
        $modelId = $this->getData('product_id');

        $query = "DELETE FROM $tableName  WHERE attribute_id = $attributeId AND product_id = $modelId";
        $db->delete($query);
    }

    protected function getModelAttributes($attributeSetId)
    {
        $db = Connector::getInstance();

        $query = "
              SELECT * 
              FROM attribute 
              WHERE attribute.id IN (
                  SELECT attribute_id 
                  FROM attribute_set_data 
                  WHERE attribute_set_id = $attributeSetId
              )";

        $result = $db->query($query);
        return $result->getCollection();
    }

    protected function validateAttributes($attributes)
    {
        foreach ($attributes as $attribute)
        {
            $type = $attribute->getData('attribute_type');
            $code = $attribute->getData('code');
            $value = $this->getData($code);

            $errorMessage = "";

            if(empty($value))
            {
                throw new Exception("$code is empty");
            }

            switch ($type)
            {
                case 'double':
                    if(!$this->isDouble($value))
                    {
                        $errorMessage = "error";
                    }
                break;
                case 'int':
                    if(!is_numeric($value))
                    {
                        $errorMessage = "error";
                    }
                break;
                case 'varchar':
                    if(!(is_string($value) && strlen($value) > 0))
                    {
                        $errorMessage = "error";
                    }
                break;
                default;
            }
            if($errorMessage == "error")
            {
                throw new Exception("Problem with data: $code");
            }
        }

    }

    public function isDouble($value)
    {
        $floatVal = floatval($value);
        return is_numeric($value) || $floatVal && intval($floatVal) != $floatVal;
    }

    public function delete()
    {
        $entityId = $this->getData('product_id');
        $attributeSetId = $this->getData('attribute_set_id');

        $attributes = $this->getModelAttributes($attributeSetId);

        foreach ($attributes as $attribute)
        {
            $this->deleteAttribute($attribute);
        }

        $db = Connector::getInstance();

        $query = "DELETE FROM {$this->baseTable} WHERE product_id = $entityId";
        $db->delete($query);
    }
}