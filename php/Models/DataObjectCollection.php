<?php

class DataObjectCollection extends DataObject
{
    private $collection = array();

    public function getCollection()
    {
        return $this->collection;
    }

    public function setCollection($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function addItem($item)
    {
        $this->collection[] = $item;
        return $this;
    }

    public function firstItem()
    {
        if(count($this->collection) > 0)
        {
            return $this->collection[0];
        }

        return null;
    }

    public function getCount()
    {
        return count($this->collection);
    }
}