<?php

class Connector
{
    private static $instance = null;

    private $host = null;
    private $dbUser = null;
    private $dbPassword = null;
    private $database = null;
    private $connection = null;

    private function __construct()
    {
        $xml = simplexml_load_file(PROJECT_DIR."etc/config.xml");

        $mysqlSettings = $xml->mysql[0];

        $this->host = (string)$mysqlSettings->dbhost;
        $this->dbUser = (string)$mysqlSettings->dbuser;
        $this->dbPassword = (string)$mysqlSettings->dbpassword;
        $this->database = (string)$mysqlSettings->database;

        $this->connection = mysqli_connect($this->host, $this->dbUser, $this->dbPassword, $this->database);

        if($this->connection->connect_error)
        {
            var_dump('database failed to connect');
        }
    }

    public function __destruct()
    {
        mysqli_close($this->connection);
        $this->connection = null;
    }

    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function query($query)
    {
        $result = $this->connection->query($query);
        $collection = new DataObjectCollection();
        while($row = $result->fetch_assoc())
        {
            $dataObject = new DataObject();
            $row = array_filter($row, function($value){ return !is_null($value);} );
            $dataObject->addData($row);
            $collection->addItem($dataObject);
        }
        return $collection;
    }

    public function insert($query)
    {
        if ($this->connection->query($query) === true)
        {
            $lastId = $this->connection->insert_id;
            return $lastId;
        }
        else
        {
            throw new Exception($this->connection->error);
        }
    }

    public function rawSqlQuery($query)
    {
        if($this->connection->query($query) === true)
        {
            return $this->connection;
        }
        else
        {
            throw new Exception($this->connection->error);
        }
    }

    public function delete($query)
    {
        if ($this->connection->query($query) !== true)
        {
            throw new Exception($this->connection->error);
        }
    }

    public function realEscapeString($string)
    {
        return $this->connection->real_escape_string($string);
    }

}